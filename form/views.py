from django.shortcuts import render, get_object_or_404
from .models import PersonalAccount, RepresentativeAccount, Beneficiary
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import Http404, HttpResponse
from django.views.generic import View
from django.template.loader import get_template
from .utils import render_to_pdf
from .forms import PersonalAccountForm, RepresentativeAccountForm
from easy_pdf.views import PDFTemplateView
from django.conf import settings

# Create your views here.
# def index(request):
#     all_users = PersonalAccount.objects.filter(pk=1)
#     return render(request, 'form/index.html', {'all_users': all_users})
def index(request):
    latest_forms = PersonalAccount.objects.order_by('-name')[:5]
    latest_forms_company = RepresentativeAccount.objects.order_by('-representative_name')[:5]
    context = {'latest_forms': latest_forms, 'latest_forms_company' : latest_forms_company}
    return render(request, 'form/index.html', context)

def detail(request, form_id):
    form = get_object_or_404(PersonalAccount, pk=form_id)
    return render(request, 'form/single.html', {'form': form})

def detail_company(request, form_id):
    form = get_object_or_404(RepresentativeAccount, pk=form_id)
    return render(request, 'form/single_company.html', {'form': form})


def signup(request):
    all_users = PersonalAccount.objects.all()
    context = {'all_data': all_users}
    return render(request, 'form/index.html', context)

def saveform(request):
    all_users = get_object_or_404(PersonalAccount.objects.all())
    return render(request, 'form/base.html', {'all_data': all_users})

class SignupForm(CreateView):
    model = PersonalAccount
    form_class = PersonalAccountForm

class SignupFormLegal(CreateView):
    model = RepresentativeAccount
    form_class = RepresentativeAccountForm

# class GeneratePDF(View):
#     def get(self, request, *args, **kwargs):
#         template = get_template('form/pdf.html')
#         selected_user = PersonalAccount.objects.filter(pk=5)
#         context = {
#             'all_users': selected_user
#         }
#         pdf = render_to_pdf('form/pdf.html', context)
#         if pdf:
#             response = HttpResponse(pdf, content_type='application/pdf')
#             filename = 'formNumber_%s.pdf' % ('12324')
#             content = 'inline; filename="%s"' %(filename)
#             response['Content-Disposition'] = content
#             download = request.GET.get('download')
#             if download:
#                 content = "attachment; filename='%s'" %(filename)
#             response['Content-Disposition'] = content
#             return response
#         return HttpResponse('Not found')

class IndividualPDFView(PDFTemplateView):
    template_name = 'form/pdf.html'

    def get_context_data(self, **kwargs):
        id = kwargs['form']
        form = get_object_or_404(PersonalAccount, pk=id)
        return super(IndividualPDFView, self).get_context_data(
            pagesize='A4',
            title='Hi there!',
            form = form
        )

class CompanyPDFView(PDFTemplateView):
    template_name = 'form/compdf.html'

    def get_context_data(self, **kwargs):
        id = kwargs['form']
        form = get_object_or_404(PersonalAccount, pk=id)
        return super(CompanyPDFView, self).get_context_data(
            pagesize='A4',
            title='Hi there!',
            form = form
        )