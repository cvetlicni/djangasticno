# Generated by Django 2.0.1 on 2018-01-06 21:57

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personalaccount',
            name='date_handler',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 1, 6, 21, 57, 32, 549561, tzinfo=utc), verbose_name='Datum'),
        ),
        migrations.AlterField(
            model_name='personalaccount',
            name='date_signed',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 1, 6, 21, 57, 32, 549561, tzinfo=utc), verbose_name='date signed'),
        ),
        migrations.AlterField(
            model_name='representativeaccount',
            name='representative_date_handler',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 1, 6, 21, 57, 32, 551565, tzinfo=utc), verbose_name='Datum'),
        ),
        migrations.AlterField(
            model_name='representativeaccount',
            name='representative_date_signed',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 1, 6, 21, 57, 32, 551565, tzinfo=utc), verbose_name='date signed'),
        ),
    ]
