from django import forms
from django.forms import ModelForm
from django_countries.widgets import CountrySelectWidget

from .models import PersonalAccount, RepresentativeAccount, Beneficiary


class DateInput(forms.DateInput):
    input_type = 'date'


class PersonalAccountForm(ModelForm):

    class Meta:
        model = PersonalAccount
        fields = ['name', 'surname', 'citizenship', 'birthdate', 'birthplace', 'address', 'temporary_address', 'id_type', 'id_number', 'expiry_date', 'tax_number', 'phone_number', 'email', 'bank_account',
              'employment_status', 'position', 'trade_account']
        widgets = {
            'birthdate': DateInput(),
            'expiry_date': DateInput(),
            'citizenship': CountrySelectWidget(),

        }

class RepresentativeAccountForm(ModelForm):

    class Meta:
        model = RepresentativeAccount
        fields = ['representative_name', 'representative_surname', 'representative_citizenship', 'representative_birthdate', 'representative_birthplace', 'representative_address',
                  'representative_id_type', 'representative_id_issuer', 'representative_id_number', 'representative_id_issued', 'representative_tax_number', 'representative_date_registered', 'representative_rights',
                  'representative_trade_account', 'company_name', 'company_address', 'company_branch_address', 'company_registration_number', 'company_vat', 'company_bank_account']
        widgets = {
            'representative_birthdate': DateInput(),
            'representative_id_issued': DateInput(),
            'representative_date_registered': DateInput(),
            'representative_citizenship': CountrySelectWidget(),
        }