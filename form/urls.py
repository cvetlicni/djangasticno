from django.urls import path, include
from django.contrib import admin
from . import views

app_name='form'
urlpatterns = [
    path('', views.index, name='index'),
    path('individual/<int:form_id>/', views.detail, name='detail'),
    path('company/<int:form_id>/', views.detail_company, name='deets'),
    path('form-individual/', views.SignupForm.as_view(), name='form'),
    path('form-company/', views.SignupFormLegal.as_view(), name='company'),
    path('saved/', views.saveform, name='saved'),
    # path('pdf/', views.GeneratePDF.as_view(), name='pdf'),
    path('<int:form>/pdf/', views.IndividualPDFView.as_view(), name='pdf'),
    path('<int:form>/compdf/', views.CompanyPDFView.as_view(), name='compdf'),
]
