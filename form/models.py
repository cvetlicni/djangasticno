from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField
from django.utils import timezone


# Create your models here.

ID_CHOICES = (
    ('passport', 'Passport'),
    ('id_card', 'ID card'),
    ('drivers_license', 'Drivers license'),
)
ID_ISSUER = (
    ('country', 'Country'),
    ('dmw', 'DMW'),
)
class PersonalAccount(models.Model):
    name = models.CharField('name', max_length=200, default='')
    surname = models.CharField('surname', max_length=200, default='')
    citizenship = CountryField('citizenship', blank_label='')
    birthdate = models.DateTimeField('birth date')
    birthplace = models.CharField('birth place', max_length=200)
    address = models.CharField('address (street, No., ZIP, City, Country)', max_length=400, blank=True)
    temporary_address = models.CharField('temporary address (street, No., ZIP, City, Country)', max_length=200, blank=True)
    id_type = models.CharField('type of id', max_length=200, choices=ID_CHOICES)
    id_number = models.CharField('ID number', max_length=200)
    expiry_date = models.DateTimeField('expiry date')
    tax_number = models.CharField('tax no.', max_length=200)
    phone_number = models.CharField('phone no.', max_length=200)
    email = models.EmailField('email')
    bank_account = models.CharField('bank acc no. (to deal with Bitnik)', max_length=200)

    employment_status = models.CharField('employment status', max_length=200)
    position = models.CharField('position/activity', max_length=200)

    trade_account = models.BooleanField('Opening a trade account', default=False)

    signature_customer = models.BooleanField('signature', blank=True, default=False)
    date_signed = models.DateTimeField('date signed', blank=True, default=timezone.now())
    place_signed = models.CharField('place signed', max_length=200, blank=True, default='')

    risk = models.CharField('Oznaka kategorije tveganja (po ZPPDFT-1)', max_length=200, default='', blank=True)
    politician = models.BooleanField('politično izpostavljena oseba', blank=True, default=False)
    remarks = models.TextField('Opombe', blank=True, default='')
    name_handler = models.CharField('Ime in priimek referenta', max_length=200, blank=True, default='')
    signature_handler = models.BooleanField('Podpis referenta', blank=True, default=False)
    date_handler = models.DateTimeField('Datum', blank=True, default=timezone.now())
    place_handler = models.CharField('Kraj', max_length=200, default='', blank=True)

    def get_absolute_url(self):
        return reverse('form:index')

    def __str__(self):
        return self.name + ' ' + self.surname + ' - ' + self.email


class RepresentativeAccount(models.Model):
    representative_name = models.CharField('name', max_length=200, default='')
    representative_surname = models.CharField('surname', max_length=200, default='')
    representative_citizenship = CountryField('citizenship', blank_label='')
    representative_birthdate = models.DateTimeField('birth date')
    representative_birthplace = models.CharField('birth place', max_length=200)
    representative_address = models.CharField('address (street, No., ZIP, City, Country)', max_length=200)
    representative_id_type = models.CharField('type of id', max_length=200, choices=ID_CHOICES)
    representative_id_issuer = models.CharField('id issuer', max_length=200, choices=ID_ISSUER)
    representative_id_number = models.CharField('ID number', max_length=200)
    representative_id_issued = models.DateTimeField('id date issued')
    representative_tax_number = models.CharField('tax number', max_length=200)
    representative_date_registered = models.CharField('date registered as an procurist', max_length=200)
    representative_rights = models.TextField('what rights do you have')

    representative_trade_account = models.BooleanField('Trade account', default=False)

    representative_sign_stamp = models.BooleanField('signature and stamp', blank=True, default=True)
    representative_date_signed = models.DateTimeField('date signed', blank=True, default=timezone.now())
    representative_place_signed = models.CharField('place signed', max_length=200, default='', blank=True)

    representative_risk = models.CharField('Oznaka kategorije tveganja (po ZPPDFT-1)', max_length=200, default='', blank=True)
    representative_politician = models.BooleanField('politično izpostavljena oseba', blank=True, default=False)
    representative_remarks = models.TextField('Opombe', blank=True, default='')
    representative_name_handler = models.CharField('Ime in priimek referenta', max_length=200, blank=True, default='')
    representative_signature_handler = models.BooleanField('Podpis referenta', blank=True, default=False)
    representative_date_handler = models.DateTimeField('Datum', blank=True, default=timezone.now())
    representative_place_handler = models.CharField('Kraj', max_length=200, default='', blank=True)

    company_name = models.CharField('Company name', max_length=200)
    company_address = models.CharField('Company HQ address', max_length=400)
    company_branch_address = models.CharField('Company branch address (if different)', max_length=400)
    company_registration_number = models.CharField('Company registration number', max_length=200)
    company_vat = models.CharField('Company VAT number', max_length=200)
    company_bank_account = models.CharField('Company bank account', max_length=200)

    def get_absolute_url(self):
        return reverse('form:index')

    def __str__(self):
        return self.representative_name + ' ' + self.representative_surname + ' - ' + self.company_name


class Beneficiary(models.Model):
    representative_account = models.ForeignKey(RepresentativeAccount, on_delete=models.CASCADE)
    beneficiary_name = models.CharField('birthplace', max_length=200)
    beneficiary_surname = models.CharField('birthplace', max_length=200)
    beneficiary_address = models.CharField('birthplace', max_length=200)
    beneficiary_tax = models.CharField('birthplace', max_length=200)
    beneficiary_citizenship = models.CharField('birthplace', max_length=200)
    beneficiary_relation = models.CharField('birthplace', max_length=200)
    beneficiary_relation_percentage = models.FloatField('birthplace')