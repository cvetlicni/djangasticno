from django.urls import path, re_path

from . import views

app_name = 'polls'
urlpatterns = [ 
  path("", views.index, name = "index"),
  re_path("redirected/", views.redirected, name = "redirected"),
  path("neki/", views.neki, name = "neki"),
  path("logout/", views.logout, name='logout'),

]