from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from oauth2client.client import OAuth2WebServerFlow
from django.contrib.auth.models import User
import httplib2
import json
from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import logout as auth_logout

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return render(request, 'form/index.html', {'logout': 'You are not logged out'})

def index(request):
  flow = getFlow()
  authUri = flow.step1_get_authorize_url()
  template = loader.get_template("polls/index.html")
  context = {
    "linkToGoogle": authUri
  }
  return HttpResponse(template.render(context, request))

def redirected(request):
  template = loader.get_template("polls/index.html")
  try:
    code = request.GET['code']
  except KeyError:
    code = ""
  try:
    err = request.GET['error']
  except KeyError:
    err = ""
  if code:
    flow = getFlow()
    credentials = flow.step2_exchange(code)
    jason = credentials.to_json()
    jason = json.loads(jason)
    username = jason['id_token']['email']
    firstname = jason['id_token']['given_name']
    lastname = jason['id_token']['family_name']
    try:
      testuser=User.objects.all()
      user = User.objects.get(username=username)
    except ObjectDoesNotExist:
      # user = User.objects.create_user(username, username, username + "asdf")
      # user.first_name = firstname
      # user.last_name = lastname
      # user.save()
      context = {
        'not_found': 'Your account was not found in the DB.'
      }
      return HttpResponse(template.render(context, request))
    authUser = authenticate(request, username = username, password = username + "asdf")
    print("auth user", authUser)
    login(request, authUser)
    return  HttpResponseRedirect("/polls/neki")
  return HttpResponse("neki pise tm ")

def getFlow():
  return OAuth2WebServerFlow(
          client_id="838728241793-11o6gg1ff636urd50agjg6s5n91ta7s6.apps.googleusercontent.com",
          client_secret="H1a3f-CwCWsQ1ZJPD3ecw9Hn",
          redirect_uri="http://localhost:8000/polls/redirected",
          scope=["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"])

def complete(request):
  print (request.GET[''])

def neki(request):
  print(request.user)
  template = loader.get_template("polls/index.html")
  if request.user.is_authenticated:
    context = {
      'success': 'Successfully logged in!'
    }
    return HttpResponse(template.render(context, request))
  context = {
    'not_found': 'Successfully logged in!'
  }
  return HttpResponse(template.render(context, request))
# Create your views here.
