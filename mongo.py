import logging

import re


import pymongo

from bson import code


from talenton import settings


ASC = pymongo.ASCENDING

DSC = pymongo.DESCENDING


# MONGO_DB_ENDPOINT_URL = 'mongodb'

# MONGO_DB_ENDPOINT_PORT = 27017


logger = logging.getLogger(__name__)

db = pymongo.MongoClient(settings.MONGO_DB_ENDPOINT_URL, settings.MONGO_DB_ENDPOINT_PORT)


# all_count = mongo.db.database_name.collection_name.find().count()

# gathered_count = mongo.db.database_name.collection_name.find({'linkedInQueried': True}).count()

# parsed_talents_count = mongo.db.database_name.collection_name.find({'linkedInParsedLatestDate': {'$exists': True}}).hint([('linkedInParsedLatestDate', 1)]).count()

# skill_counts = json.dumps(list(mongo.db.database_name.collection_name.find({'value': {'$gte': 100}}).sort('value', mongo.DSC)))





def get_map_reduce(name):

	with open('api/mongojs/{}_map.js'.format(name), 'r') as map_file:

		map_js = code.Code(re.sub(r'^\(|\);$|\)$', '', map_file.read()))



	with open('api/mongojs/{}_reduce.js'.format(name), 'r') as reduce_file:

		reduce_js = code.Code(re.sub(r'^\(|\);$|\)$', '', reduce_file.read()))



	# Use this as *args for map_reduce method on collection

	return map_js, reduce_js, name

